//generic dom functions
var DM = {};
//remove all child elements from a list
DM.removeChildren = function(parent){
	while(parent.hasChildNodes()){
        parent.removeChild(parent.firstChild);
    }
};
//typically, first and second are wrap divs
DM.swapChildUp = function(parent, child){
	var previousSibling = child.previousSibling;
	if(previousSibling !== null){
		var tempChild = parent.removeChild(child);
		parent.insertBefore(tempChild, previousSibling);
	}
};
DM.swapChildDown = function(parent, child){
	var nextSibling = child.nextSibling;
	var tempChild = parent.removeChild(child);
	if(nextSibling === null){
		//last in the list, append to parent
		parent.appendChild(tempChild);
	}else{
		parent.insertBefore(tempChild, nextSibling.nextSibling);
	}
};
//create and return a dom element
DM.genEl = function(tag, innerHTML, id, cls){
	var el = document.createElement(tag);
        if(innerHTML != null){
            el.innerHTML = innerHTML;
        }
        if(id != null){
            el.id = id;
        }
        if(cls !=null){
            el.setAttribute('class', cls);
        }
        return el;
};
//creates and returns an anchor element, href required
DM.genAnchor = function(href, innerHTML, id, cls){
        var a = document.createElement('a');
        a.href = href;
        if(innerHTML != null){
            a.innerHTML = innerHTML;
        }
        if(id != null){
            a.id = id;
        }
        if(cls != null){
            a.setAttribute('class', cls);
        }
        return a;
}, 
//creates and returns an image element, src required
DM.genImage = function(src, cls){
	var img = document.createElement('img');
        img.src = src;
        if(cls != null){
            img.setAttribute('class', cls);
        }
        return img;
};
//creates and appends an array of content to a list
DM.genList = function(parent, srcArr, cls){
	var i = 0,
	length = srcArr.length,
	el;
	for(i; i < length; i++){
		el = this.genEl('li', srcArr[i], null, cls);	
		parent.appendChild(el);
	}
};
//creates and appends a number of divs to a parent
DM.genDivs = function(parent, srcArr, cls){
	var i = 0,
	length = srcArr.length,
	el;
	for(i; i < length; i++){
		el = this.genEl('div', srcArr[i], cls);
		parent.appendChild(el);
	}	
};
//creates and returns an array of anchor elements
DM.genAnchors = function(parent, rootId, hrefArr, innerHTMLArr, cls){
	var i = 0,
	length = hrefArr.length,
	results = [],
	id = rootId,
	el;
	for(i; i < length; i++){
		id = id + i;
		el = this.genAnchor(hrefArr[i], innerHTMLArr[i], id, cls);
		parent.appendChild(el);
		el = document.getElementById(rootId);
		results.push(el); 
	}
	return results;
};

//DM.genFloatingInput = function(id, xPos, yPos, selWidth, selHeight){
//	var divId = id + ' txt',
//	div = DM.genEl('div', null, divId, null);
//	div.style.position = 'absolute';
//	div.style.top = yPos + 'px'; 
//	div.style.left = xPos + 'px';
//	div.style.width = selWidth + 'px';
//	div.style.height = selHeight + 'px';
//	
//	//txtEl.style.zIndex = 1000;
//	//set the absolute positioning of the element based on the parent offset and the selection tool's position
//	document.body.appendChild(div);
//	div = document.getElementById(divId);
//	
//	console.log('one');
//	
//	var txtWrapId = id + 'txtWrap',
//	optionsWrapId = id + 'optionsWrap',
//	txtWrap = DM.genEl('div', null, txtWrapId, null),
//	optionsWrap = DM.genEl('div', null, optionsWrapId, null);
//	optionsWrap.style.height = '25px';
//	
//	div.appendChild(txtWrap);
//	div.appendChild(optionsWrap);
//	txtWrap = document.getElementById(txtWrapId);
//	optionsWrap = document.getElementById(optionsWrapId);
//	
//	//create a text input element\
//	var txtId = id + 'txtInput',
//	txtEl = DM.genEl('input', null, txtId, null);
//	//set all positioning attributes
//	txtEl.setAttribute('type', 'text');
//	console.log(selWidth);
//	txtEl.setAttribute('size', selWidth * (1/8));
//	txtWrap.appendChild(txtEl);
//
//	//create the okay button
//	var okayId = id + 'okay',
//	okay = DM.genEl('a', null, okayId, 'txtOptions');
//	okay.innerText = 'Okay';
//	optionsWrap.appendChild(okay);
//	okay = document.getElementById(okayId);
//	
//	//create the cancel button
//	var cancelId = id + 'cancel',
//	cancel = DM.genEl('a', null, cancelId, 'txtOptions');
//	cancel.innerText = 'Cancel';
//	optionsWrap.appendChild(cancel);
//	cancel = document.getElementById(cancelId);
//};


DM.getElementPosition = function(el){
	var offsetLeft = 0, offsetTop = 0;
	if (el.offsetParent) {
		do{
			//console.log('setting');
			offsetLeft += el.offsetLeft;
			offsetTop += el.offsetTop;
		}while(el = el.offsetParent);
	}
	return [offsetLeft, offsetTop];
};