//draw Tools
function DrawTool(){
	this.focusItem = null;
};
//clears the canvas
DrawTool.prototype.clearCanvas = function(){
	var item = this.focusItem;
	item.context.clearRect(0, 0, item.canvas.width, item.canvas.height);
};
//reset a item's x and y crop values
DrawTool.prototype.resetXY = function(){
	var item = this.focusItem;
	item.x = 0;
	item.y = 0;
	item.secondX = 0;
	item.secondY = 0;
};
DrawTool.prototype.setHeightAndWidth = function(data){
	var item = this.focusItem;
	item.canvas.height = data.height;
	item.canvas.width = data.width;
};

//a draw actor is a tool which has continous listeners attached to a canvas
function DrawActorTool(){};
inherit(DrawActorTool, DrawTool);
DrawActorTool.prototype.removeAllListeners = function(){
	var canvas = this.focusItem.canvas,
	i = 0,
	length = this.handlers.length,
	handlerObj;
	for(i; i < length; i++){
		handlerObj = this.handlers[i];
		canvas.removeEventListener(handlerObj.name, handlerObj.handler, handlerObj.bubble);
	}
};

function PenTool(){
	this.handlers = [];
};
inherit(PenTool, DrawActorTool);
function SelectionTool(){
	this.handlers = [];
};
inherit(SelectionTool, DrawActorTool);

function CropTool(){};
inherit(CropTool, DrawTool);

//sort tools
function SortTool(){};
//TODO: better index checking
SortTool.prototype.sortArrUp = function(arr, index){
	if(arr[index - 1]){
		var temp = arr[index - 1];
		arr[index - 1] = arr[index];
		arr[index] = temp;
	}
};
//TODO: better index checking
SortTool.prototype.sortArrDown = function(arr, index){
	if(arr[index + 1]){
		var temp = arr[index + 1];
		arr[index + 1] = arr[index];
		arr[index] = temp;
	}
};

PenTool.prototype.turnPenOn = function(){
	var item = this.focusItem;
	var boundMousedownHandler = this.mousedownHandler.bind(this);
	item.canvas.addEventListener('mousedown', boundMousedownHandler, false);
	this.handlers.push({name: 'mousedown', handler: boundMousedownHandler, bubble: false});
	
	var boundMousemoveHandler = this.mousemoveHandler.bind(this);
	item.canvas.addEventListener('mousemove', boundMousemoveHandler, false);
	this.handlers.push({name: 'mousemove', handler: boundMousemoveHandler, bubble: false});
	
	var boundMouseupHandler = this.mouseupHandler.bind(this);
	item.canvas.addEventListener('mouseup', boundMouseupHandler, false);
	this.handlers.push({name: 'mouseup', handler: boundMouseupHandler, bubble: false});
};
PenTool.prototype.mousedownHandler = function(e){
	var item = this.focusItem;
	item.penX.push(e.offsetX);
	item.penY.push(e.offsetY);
	item.drawOn = true;
};
PenTool.prototype.mousemoveHandler = function(e){
	var item = this.focusItem;
	if(item.drawOn){
		item.penX.push(e.offsetX);
		item.penY.push(e.offsetY);
		this.clearCanvas();
		item.context.putImageData(item.dataObjects[item.dataObjects.length - 1], 0, 0);
		item.context.lineJoin = 'round';
		var i = 0, length = item.penX.length;
		for(i; i < length; i++){
				item.context.beginPath();
				item.context.moveTo(item.penX[i - 1], item.penY[i - 1]);
				item.context.lineTo(item.penX[i], item.penY[i]);
				item.context.closePath();
				item.context.stroke();
		}
	}
};
PenTool.prototype.mouseupHandler = function(e){
	var item = this.focusItem;
	var imageData = item.context.getImageData(0, 0, item.canvas.width, item.canvas.height);
	item.dataObjects.push(imageData);
	item.penX.splice(0, item.penX.length);
	item.penY.splice(0, item.penY.length);
	item.drawOn = false;
};


SelectionTool.prototype.turnOnRectangle = function(){
		var item = this.focusItem;
		//set stroke style to refresh ( in case of colored pen )
        item.context.strokeStyle = '#000';
		
		var boundMousedownHandler = this.mousedownHandler.bind(this);
        item.canvas.addEventListener('mousedown', boundMousedownHandler, false);
		this.handlers.push({name: 'mousedown', handler: boundMousedownHandler, bubble: false});
		
		var boundMousemoveHandler = this.mousemoveHandler.bind(this);
        item.canvas.addEventListener('mousemove', boundMousemoveHandler, false);
		this.handlers.push({name: 'mousemove', handler: boundMousemoveHandler, bubble: false});
		
		var boundMouseupHandler = this.mouseupHandler.bind(this);
        item.canvas.addEventListener('mouseup', boundMouseupHandler, false);
		this.handlers.push({name: 'mouseup', handler: boundMouseupHandler, bubble: false});
};
SelectionTool.prototype.mousedownHandler = function(e){
	var item = this.focusItem;
	item.x = e.offsetX;
	item.y = e.offsetY;
	item.drawOn = true;
};
SelectionTool.prototype.mousemoveHandler = function(e){
	var item = this.focusItem;
	if(item.drawOn){
		item.secondX = e.offsetX;
		item.secondY = e.offsetY;
		this.clearCanvas();
		item.context.putImageData(item.dataObjects[item.dataObjects.length - 1], 0, 0);
		item.context.strokeRect(item.x, item.y, item.secondX - item.x, item.secondY - item.y);
	}
};
SelectionTool.prototype.mouseupHandler = function(e){
	var item = this.focusItem;
	item.drawOn = false;
};

CropTool.prototype.doCrop = function(){
	var item = this.focusItem;
	var imageData = item.context.getImageData(item.x, item.y, item.secondX - item.x, item.secondY - item.y); 
	item.dataObjects.push(imageData);
	this.clearCanvas(); 
	this.setHeightAndWidth(imageData);
	item.context.putImageData(imageData, 0, 0);
	item.hasBeenCropped = true;
	this.resetXY();
};

//TODO: swap all current tool functions that add listeners to a canvas to accept only a canvas element as a parameter
//optional callback? what would it be used for?

function TextTool(){
	//text tool gets hooks to dom elements
	this.wrap = null;
	this.input = null;
	this.handlers = [];
};
inherit(TextTool, DrawTool);
//creates a text box at the given selection coordinates
//TextTool.prototype.createTextInput = function(){
//	var item = this.focusItem,
//	selWidth = (item.secondX - item.x),
//	selHeight = (item.secondY - item.y),
//	//get the canvas element's absolute position in the DOM
//	coords = DM.getElementPosition(item.canvas);
//	
//	var xPos = (coords[0] + item.x);
//	var yPos = (coords[1] + item.y);
//	
//	var controls = DM.genFloatingInput(item.id, xPos, yPos, selWidth, selHeight);
//	
//};
TextTool.prototype.createTextInput = function(){
	var item = this.focusItem,
	inputId = item.id + 'txtInput',
	input = DM.genEl('input', null, inputId, null);
	input.setAttribute('size', '100');
	item.wrap.insertBefore(input, item.canvas);
	input = document.getElementById(inputId);
	this.input = input;
	
	//bind a listener
	var boundInputSubmitHandler = this.inputSubmitHandler.bind(this);
	input.addEventListener('keypress', boundInputSubmitHandler, false);
	this.handlers.push({name: 'keypress', handler: boundInputSubmitHandler, bubble: false});
};
TextTool.prototype.inputSubmitHandler = function(e){
	if(e.keyCode === 13){
		//INVARIANT: enter was pressed
		//keep the text input
		
		//paint the text on the canvas, and hook dragging
		this.addTextToCanvas(this.focusItem.canvas, this.input.value, 0, 0);
	}
};
//adds text to the given canvas at the given coordinates and turns on drag listeners
TextTool.prototype.addTextToCanvas = function(canvas, text, topLeftCornerX, topLeftCornerY){
	//uses x, y, and the difference between x y and their second x, y properties
	var height = 30; //height
	//position, baseline top
	var topLeftCornerX = 50;
	var topLeftCornerY = 50;
	
	context.fillStyle = '#f00';
	//ar metrics = context.measureText(val);
	context.font = height + 'px sans-serif';
	context.textBaseline = 'top';
	context.fillText(val, topLeftCornerX, topLeftCornerY);
	
	//this has to occur after context.font is set to a px size
	var metrics = context.measureText(val);
	var textWidth = metrics.width;
	
	//context.strokeStyle = "#000";
	//context.strokeRect(topLeftCornerX, topLeftCornerY, metrics.width, height);
	
	//adjust these functions to accept checks as passed params
	function checkX(offset){
		return (offset >= topLeftCornerX && offset <= (topLeftCornerX + textWidth));
	}
	function checkY(offset){
		return (offset >= topLeftCornerY && offset <= (topLeftCornerY + height));
	}
	
	var mouseMoveOk = false;
	
	//this needs to be throttled
	canvas.addEventListener('mousedown', function(e){
		console.log('about to create');
		//calculate offset with canvas from document height and width
		//build as a floating div with option: done, cancel
		
		//text input should be sized accoridng to the given font size, and the selected rect width on the canvas
		var txtEl = document.createElement('input');
		txtEl.setAttribute('size', 100);
		txtEl.style.position = 'absolute';
		txtEl.style.top = e.offsetY + 'px';
		txtEl.style.left = e.offsetX + 'px';
		txtEl.style.zIndex = 1000;
		document.body.appendChild(txtEl);
		console.log('done creating');
		//console.log('mousedown');
		//check if mousedown was inside the box
		//if(checkX(e.offsetX) && checkY(e.offsetY)){
		//	//set bool to allow mouse move
		//	mouseMoveOk = true;
		//	canvas.removeEventListener('mousedown', this, false);
		//}
	}, false);
	canvas.addEventListener('mousemove', function(e){
		console.log('mousemove');
		if(mouseMoveOk){
			context.clearRect(0, 0, canvas.width, canvas.height);
			context.fillText(val, e.offsetX, e.offsetY);
			context.strokeRect(e.offsetX, e.offsetY, textWidth, height);
		}
	}, false);
	canvas.addEventListener('mouseup', function(e){
		mouseMoveOk = false;
		
	}, false);
};
