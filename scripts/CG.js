﻿var CG = new Glue();
CG.setup = function(){
	var me = this;
	this.addPublications();
	//this.addSubscriptions();
	
	//setup mainList
	this.mainList = document.getElementById('editorMainList');
	
	//get main list wrap and set focus handler
	this.mainListWrap = document.getElementById('editorMainWrap');
	this.mainListWrap.addEventListener('click', function(e){
		if(me.focus !== null){
			me.resetFocus();
		}
	}, false);
	
	//setup tools
	this.setupSelectionTool();
	this.setupCropTool();
	this.setupPenTool();
	this.setupAddToCombinatorTool();
	this.setupSaveTool();
	this.setupDestroyTool();
	//this.setupUploadTool();		
};
CG.addPublications = function(){
	this.addEvent('addToCombinator');
}
//CG.addSubscriptions = function(){}
CG.setupSelectionTool = function(){
	var selectionEl = document.getElementById('selection');
	var selectionTool = new SelectionTool();
	selectionTool.el = selectionEl;
	var me = this;
	selectionTool.el.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.setToolFocus('selection', selectionTool);
			me.selectionTool.focusItem = me.focus;
			me.selectionTool.turnOnRectangle();
			me.selectionTool.el.style.borderColor = DARKGREY;
			_gaq.push(['_trackEvent', 'selectionTool', 'clicked']);
		}
	}, false);
	me.selectionTool = selectionTool;
};
CG.setupCropTool = function(){
	//get tools and set focus handler
	var cropEl = document.getElementById('crop');
	var cropTool = new CropTool();
	cropTool.el = cropEl;
	var me = this;
	cropTool.el.addEventListener('click', function(){
		//we can check toolFocus because undefined returns falsy
		if(!isVoid(me.toolFocus) && me.toolFocus.name === 'selection'){
			if(!isVoid(me.focus)){
				me.checkResetToolFocus();
				//because the crop tool doesn't have any event handlers, we never set toolFocus on him
				me.cropTool.focusItem = me.focus;
				me.cropTool.doCrop();
				me.focus.wrap.style.width = me.focus.canvas.style.width;
				me.resetFocus();
				_gaq.push(['_trackEvent', 'cropTool', 'clicked']);
			}
		}
	}, false);
	me.cropTool = cropTool;
};
CG.setupPenTool = function(){
	var me = this;
	var penEl = document.getElementById('pen');
	var penTool = new PenTool();
	penTool.el = penEl;
	penTool.el.addEventListener('click', function(e){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.setToolFocus('pen', me.penTool);
			me.penTool.focusItem = me.focus;
			me.penTool.turnPenOn();
			_gaq.push(['_trackEvent', 'penTool', 'clicked']);
		}
	}, false);
	this.penTool = penTool;
};
CG.setupAddToCombinatorTool = function(){
	var me = this;
	var addToCombinatorEl = document.getElementById('addToCombinator');
	addToCombinatorEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var item = me.focus;
			var height = item.canvas.height;
			var width = item.canvas.width;
			//get the current state of the image
			var imageData = item.context.getImageData(0, 0, width, height);
			me.fireEvent('addToCombinator', {imageData: imageData, height: height, width: width});
			me.resetFocus();
			_gaq.push(['_trackEvent', 'addToCombinatorTool', 'clicked']);
		}
	}, false);
	this.addToCombinatorEl = addToCombinatorEl;
};
//TODO save
CG.setupSaveTool = function(){
	var me = this;
	var saveEl = document.getElementById('save');
	saveEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.convertToPNG();
			_gaq.push(['_trackEvent', 'saveTool', 'clicked']);
		}
	}, false);
	this.saveEl = saveEl;
};
CG.setupDestroyTool = function(){
	var me = this;
	var destroyEl = document.getElementById('destroy');
	destroyEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.destroyFocusItem();
			_gaq.push(['_trackEvent', 'destroyTool', 'clicked']);
		}
	}, false);
	this.destroyEl = destroyEl;
};
CG.setupUploadTool = function(){
	var me = this;
	var upload = document.getElementById('getURL');
	upload.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			var item = me.focus;
            var dataURL = item.canvas.toDataURL();
			//outgoing data
			var dataChunk = {
				dataURL: dataURL,
				sourceURL: item.sourceURL,
				pageTitle: item.pageTitle
			};
			//form an http request to the server.
			var url = 'http://canvasglue.com/upload';
			createPOSTRequest(url, dataChunk, function(responseText){
				var id = item.id + 'imageLink';
				var anchor = DM.genAnchor(responseText, responseText, id, null);
				item.wrap.insertBefore(anchor, item.canvas);
			});
			_gaq.push(['_trackEvent', 'uploadTool', 'clicked']);
		}
	}, false);
};
//returns a blank item object
CG.genItem = function(){
	var item = {
		id: this.itemCounter,
		dataURL: null,
		x: 0,
		y: 0,
		secondX: 0,
		secondY: 0,
		penX: [],
		penY: [],
		varsSet: false,
		penVarsSet: false,
		drawOn: false,
		cropOn: false,
		combinatorNum:0,
		dataObjects: [],
		wrap: null,
		canvas: null,
		context: null
				
	};
	++this.itemCounter;
	return item;
};
CG.genBlank = function(){
	var item = this.genItem();
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawBlankImage(item);
};
CG.genWholePage = function(dataURLArr, sourceURL, pageTitle){
	var i = 0, length = dataURLArr.length;
	for(i; i < length; i++){
		this.genScreenshot(dataURLArr[i], sourceURL, pageTitle);
	}
};
CG.genScreenshot = function(dataURL, sourceURL, pageTitle){
	var item = this.genItem();
	item.dataURL = dataURL;
	item.sourceURL = sourceURL;
	item.pageTitle = pageTitle;
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawImage(item);
	_gaq.push(['_trackEvent', 'genScreenshot', 'screenshotGenerated']);
};
CG.drawImage = function(item){
	var me = this;
	
	//get the canvas context
	var context = item.canvas.getContext('2d');
	item.context = context;
	
	var image = new Image();
	image.onload = function(){
		var width = image.width;
		var height = image.height;
		item.wrap.style.height = height;
		item.wrap.style.width = width;
		item.canvas.height = height;
		item.canvas.width = width;
		context.drawImage(image, 0, 0);
		//save image state and item
		me.saveItem(item);
		//clear the dataUrl
		delete me.dataUrl;
	};
	image.src = item.dataURL;
};
CG.drawBlankImage = function(item){
	var width = 900, 
	height = 900,
	context = item.canvas.getContext('2d');
	item.context = context;
	
	item.wrap.style.height = height;
	item.wrap.style.width = width; 
	item.canvas.height = height;
	item.canvas.width = width;
	
	context.fillStyle = WHITE;
	//top x left, top y left, width, height
	context.fillRect(0, 0, width, height);
	
	//save image state and item
	this.saveItem(item);
};
CG.saveItem = function(item){
	//save image state
	var imageData = item.context.getImageData(0, 0, item.canvas.width, item.canvas.height);
	item.dataObjects.push(imageData);
	
	//save item
	this.items.push(item);
};
CG.setup();