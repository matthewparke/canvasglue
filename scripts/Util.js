﻿var DARKGREY = '#303030';
var WHITE = '#FFFFFF';
var LIGHTBLUE = '#319bd2';
var OFFWHITE = '#f5f5f5';
var WINDOWHEIGHT = window.height;
var WINDOWWIDTH = window.width;

//analytics
var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22460819-2']);
  _gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

//Object.prototype.augment = function(config){
//	var index;	
//	for(index in config){
//		this[index] = config[index];
//	}
//};

//This inherit function was written incorrectly to simulate multiple inheritance...don't use this.
//Based on douglas crockfords beget object inheritance method:  
//http://javascript.crockford.com/prototypal.html
function inherit(child, parent){
	function F(properties){};

	F.prototype = parent.prototype;
	child.prototype = new F();
};

function isVoid(val){
	return (val === null) || (val === undefined);
};

function createPOSTRequest(url, data, callback){
	var request = new XMLHttpRequest();
	request.open('POST', url, true);
	request.onreadystatechange = function(event){
		console.log('ready state change');
		if(request.readyState === 4){
			//console.log('ready state 4');
			//INVARIANT: the request is finished
			if(request.status === 200){
				//console.log('request state 200');
				//INVARIANT: the request was successful
				callback(request.responseText);
			}
		}
	};
	var stringifiedData = JSON.stringify(data);
	request.send(stringifiedData);
}