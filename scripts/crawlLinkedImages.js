﻿var port = chrome.extension.connect();
var jpg = new RegExp(".jpg", "g");
var png = new RegExp(".png", "g");
var gif = new RegExp(".gif", "g");
function filter(anchors){
	var hrefs = [];
	var i = 0;
	var str;
	var jpgRes;
	var pngRes;
	var gifRes;
	while(i < anchors.length){
		str = anchors[i].href;
		if(str.charAt(str.length) != "/"){
			jpgRes = jpg.test(str);
			pngRes = png.test(str);
			gifRes = gif.test(str);
			if(jpgRes || pngRes){
				hrefs.push(str);
			}else if(gifRes){
				hrefs.push(str);
			}
			i++;
		}
	}
	return hrefs;
}
function crawlForAnchors(){
	var anchors = document.getElementsByTagName("a");
	var hrefs = filter(anchors);
	var i = 0;
	while(i < hrefs.length){
		port.postMessage({type:"crawlData", data:hrefs[i]});
		i++;
	}
	port.postMessage({type: "crawlDone"});
	anchors.splice(0, anchors.length);
	hrefs.splice(0, hrefs.length);
}
crawlForAnchors();