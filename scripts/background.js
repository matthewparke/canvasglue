var tabOpened = false;
var tabId;
var viewTabUrl;
var views;
var crawlData = [];
var scrollData = [];
var captureData;
var timeout;
var viewRefresh;

var pushOptions = {
	capture: function(){
		checkAndCreateTab();
		timeout = setTimeout(function(){
			viewRefresh = getView();
			viewRefresh.CG.genScreenshot(captureData)
		}, 1000);
	},
	scroll: function(){
		checkAndCreateTab();
		timeout = setTimeout(function(){
			viewRefresh = getView();
			viewRefresh.CG.genWholePage(scrollData);
			scrollData.splice(0, scrollData.length);
		}, 1000);
	},
	crawl: function(){
		checkAndCreateTab();
		timeout = setTimeout(function(){
			viewRefresh = getView();
			viewRefresh.CG.genWholePage(crawlData); 
			crawlData.splice(0, crawlData.length);
		}, 1000);
	},
	blank: function(){
		checkAndCreateTab();
		timeout = setTimeout(function(){
			viewRefresh = getView();
			viewRefresh.CG.genBlank();
		}, 1000);
	}
};

//popup handler map
var handlerMap = {
	capture: takeScreenshot,
	captureWhole: function(){
		chrome.tabs.executeScript(null,{file:'scripts/pageScroller.js'});
	},
	crawl: function(){
		chrome.tabs.executeScript(null,{file:'scripts/crawlLinkedImages.js'});
	},
	genBlank: pushOptions.blank
};

//content script handler map
var portHandlerMap = {
	crawlData: function(port, data){
		crawlData.push(data);
	},
	crawlDone: function(port, data){
		pushOptions.crawl();
	},
	scrollReady: function(port, data){
		takeScreenshotIncrement(port, false);
	},
	scrollDone: function(port, data){
		takeScreenshotIncrement(port, true);
	}
};
function checkAndCreateTab(){
	if(tabOpened === false){
		viewTabUrl = chrome.extension.getURL('screenshot.html');
		chrome.tabs.create({url: viewTabUrl}, function(tab){
			tabOpened = true;
			tabId = tab.id;
		});
	}
}
function getView(){
	views = chrome.extension.getViews();
	var view;
	var result;
    for (var i = 0; i < views.length; i++) {
		view = views[i];
        if (view.location.href == viewTabUrl){
			result = view;
		}
    }
	return result;
} 
function takeScreenshotIncrement(port, done){
	chrome.tabs.captureVisibleTab(null, {"format":"png"}, function(dataUrl) {
		scrollData.push(dataUrl);
	});
	if(done === false){
		port.postMessage({type:'scrollRequest'});
	}else{
		pushOptions.scroll();
	}
}
function takeScreenshot() {
	chrome.tabs.captureVisibleTab(null, {"format":"png"}, function(dataUrl) {
		captureData = dataUrl;
		pushOptions.capture();
	});
}
function loadAbout(){
	var url = chrome.extension.getURL('about.html');
	chrome.tabs.create({url: url}, function(tab){});
}

chrome.tabs.onRemoved.addListener(function(id){
	if(id === tabId){
		tabOpened = false;
	}
});
chrome.extension.onConnect.addListener(function(port) {
	port.onMessage.addListener(function(msg){
		portHandlerMap[msg.type](port, msg.data);
	});
});
chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
	handlerMap[request.type](request.data);
	sendResponse({}); // snub them.
});
