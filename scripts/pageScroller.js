﻿var port = chrome.extension.connect();
var totalHeight = document.height;
var windowHeight = window.innerHeight;
function updateScroll(){
	window.scrollBy(0, windowHeight);
	totalHeight = totalHeight - windowHeight;
}
function scrollPageDown(){
	if(totalHeight - windowHeight < 0){
		port.postMessage({type: 'scrollDone'});
	}else{
		updateScroll();
		port.postMessage({type:'scrollReady'});
	}
}
function reportReady(){
	port.postMessage({type:'scrollReady'});
	updateScroll();
}
port.onMessage.addListener(function(msg) {
	if(msg.type === 'scrollRequest'){
		scrollPageDown();
	}
});
reportReady();