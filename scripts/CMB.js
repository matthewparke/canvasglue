﻿//combinator handles everything in imageData
var CMB = new Glue();
CMB.setup = function(){
	var me = this;
	me.itemCounter = 100;
	
	//TODO: setup notifcation
	//me.notify = document.getElementById('combinatorNotify');
	//add pub sub
	//this.addPublications();
	this.addSubscriptions();
	
	this.mainList = document.getElementById('combinatorMainList');
	this.mainListWrap = document.getElementById('combinatorMainWrap');
	this.mainListWrap.addEventListener('click', function(e){
		if(!isVoid(me.Focus) && !isVoid(me.toolFocus)){
			me.resetFocus();
		}
	}, false);
	
	//setup tools
	this.setupSortTool();
	this.setupMergeTool();
	this.setupSaveTool();
	this.setupDestroyTool();
};
//CMB.addPublications = function(){}
CMB.addSubscriptions = function(){
	CG.addListener('addToCombinator', this.addImage, this);
};
CMB.setupSortTool = function(){
	var me = this;
	var sortUpEl = document.getElementById('sortUp');
	var sortDownEl = document.getElementById('sortDown');
	var sortTool = new SortTool();
	sortTool.upEl = sortUpEl;
	sortTool.downEl = sortDownEl;
	sortTool.upEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var index = me.findItem(me.focus);
			if(index !== -1){
				DM.swapChildUp(me.mainList, me.focus.wrap);
				sortTool.sortArrUp(me.items, index);
			}
			_gaq.push(['_trackEvent', 'CMBSortUpTool', 'clicked']);
		}
	}, false);
	sortTool.downEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var index = me.findItem(me.focus);
			if(index !== -1){
				DM.swapChildDown(me.mainList, me.focus.wrap);
				sortTool.sortArrDown(me.items, index);	
			}
			_gaq.push(['_trackEvent', 'CMBSortDownTool', 'clicked']);
		}
	}, false);
};
CMB.setupMergeTool = function(){
	var me = this;
	var mergeEl = document.getElementById('merge');
	mergeEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.mergeItems();
			me.resetFocus();
			_gaq.push(['_trackEvent', 'CMBMergeTool', 'clicked']);
		}
	}, false);
};
CMB.setupSaveTool = function(){
	var me = this;
	var saveEl = document.getElementById('combinatorSave');
	saveEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.convertToPNG();
			me.resetFocus();
			_gaq.push(['_trackEvent', 'CMBSaveTool', 'clicked']);
		}
	}, false);
	this.saveEl = saveEl;
};
CMB.setupDestroyTool = function(){
	var me = this;
	var destroyEl = document.getElementById('combinatorDestroy');
	//me.addEvent('destroy');
	destroyEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.destroyFocusItem();
			_gaq.push(['_trackEvent', 'CMBDestroyTool', 'clicked']);
		}
	}, false);
	this.destroyEl = destroyEl;
};
CMB.addImage = function(data){
	var item = {
		id: this.itemCounter,
		imageData: data.imageData,
		height: data.height,
		width: data.width,
		wrap: null,
		canvas: null,
		context: null
	};
	++this.itemCounter;
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawImage(item);
	this.items.push(item);
};
CMB.drawImage = function(item){
	item.canvas.height = item.height;
	item.canvas.width = item.width;
	var context = item.canvas.getContext('2d');
	item.context = context;
	
	context.putImageData(item.imageData, 0, 0);
};
CMB.mergeItems = function(){
	var i = 0,
	length = this.items.length,
	maxWidth = 0,
	totalHeight = 0,
	incrementalHeight = 0,
	item,
	dataObject;
	//TODO: check if we should remove canvases first, before removing the wrapping divs
	DM.removeChildren(this.mainList);
	
	//find max width and height
	//TODO: use array.reduce
	for(i; i < length; i++){
		item = this.items[i];
		if(item.width > maxWidth){
			maxWidth = item.width;
		}
		totalHeight = totalHeight + item.height;
	}
	
	//create a special wrap and canvas for the merge
	var wrap = DM.genEl('div', null, 'mergeWrap', 'wrap');
	wrap.style.height = totalHeight;
	wrap.style.width = maxWidth;
	this.mainList.appendChild(wrap);
	wrap = document.getElementById('mergeWrap');
	//create message
	var msg = document.createElement('p');
	msg.innerText = 'Combination status: success.';
	wrap.appendChild(msg);
	//create canvas
	var canvas = DM.genEl('canvas', null, 'mergeCanvas', null);
	canvas.height = totalHeight;
	canvas.width = maxWidth;
	canvas.style.borderColor = WHITE;
	wrap.appendChild(canvas);
	canvas = document.getElementById('mergeCanvas');
	
	var context = canvas.getContext('2d');
	i = 0;
	//TODO: extra array indices checks?
	for(i; i < length; i++){
		item = this.items[i];
		if(i === 0){
			context.putImageData(item.imageData, 0, 0);
			incrementalHeight = item.height;
		}else{
			context.putImageData(item.imageData, 0, incrementalHeight);
			incrementalHeight = incrementalHeight + item.height;
		}
	}
	
	//cleanup
	this.varCleanup();
	
	//push new item so that we can save it
	var item = {
		id: 'merged',
		imageData: context.getImageData(0, 0, canvas.width, canvas.height),
		//TODO: check if height and width are used
		height: canvas.height,
		width: canvas.width,
		wrap: wrap,
		canvas: canvas,
		context: context
	};
	this.items.push(item);
	this.checkResetToolFocus();
	this.focus = item;
	this.convertToPNG();
};
CMB.setup();