//glue is the base class for controlling lists of information
var Glue = function(mainList){
	this.mainList = null;
	this.mainListWrap = null;
	this.items = [];
	this.itemCounter = 0;
	this.focus = null;
	this.toolFocus = null;
	this.events = {}; //important!
};
inherit(Glue, Observable);
//cleanup method
Glue.prototype.varCleanup = function(){
	this.itemCounter = 0;
	this.items.splice(0, this.items.length);
};
Glue.prototype.destroyFocusItem = function(){
	var length = this.items.length,
	item = this.focus;
	item.wrap.removeChild(item.canvas);
	this.mainList.removeChild(item.wrap);
	var i = 0, found = false;
	while(i < length && found === false){
		if(this.items[i].id === item.id){
			this.items.splice(i, 1);
			found = true;
		}
		i++;
	}
	this.resetFocus();
};
//finds the first occurence
//TODO: add options to search from a certain index, or from the back of the array
//returns the index or -1
Glue.prototype.findItem = function(item){
	var i = 0,
	length = this.items.length,
	found = false,
	result = -1;
	while(i < length && !found){
		if(this.items[i].id === item.id){
			result = i;
			found = true;
		}
		i++;
	}
	return result;
};
Glue.prototype.genStructure = function(item){
	//create and append a wrap
	var wrapId = item.id + 'wrap';
	var wrap = DM.genEl('div', null, wrapId, 'wrap'); 
	this.mainList.appendChild(wrap);
	wrap = document.getElementById(wrapId);
	var me = this;
	wrap.addEventListener('click', function(e){
		e.stopPropagation();
		if(me.focus === null){
			me.setFocus(item);
			//in the future we may want additional checks here
	    		//HACK check the id of the focus, before clearing and setting
	 	}else if(me.focus.id !== item.id){
			//INVARIANT: this is not the active focus
			me.resetFocus();
			me.setFocus(item);
			//reset tool focus if canvas focus changes
			me.checkResetToolFocus();
		}
	}, false);
	return wrap;
};
//TODO: subject to change come glue types without canvas elements
Glue.prototype.genCanvas = function(item){
	//create canvas
	var canvasId = item.id + 'canvas';
	var canvas = DM.genEl('canvas', null, canvasId, null);
	canvas.style.borderColor = WHITE;
	item.wrap.appendChild(canvas);
	canvas = document.getElementById(canvasId);
	return canvas;
};
//TODO: subject to change come various glue types?
Glue.prototype.destroyItem = function(){
	var item = this.focus;
	DM.removeChildren(item.wrap);
	this.mainList.removeChild(item.wrap);
	//find the item in the collection and remove it
	var index = this.findItem(item);
	if(index >= 0){
		this.items.splice(index, 1);
	}
};
//TODO: subject to change come glue types without canvas elements
Glue.prototype.convertToPNG = function(){
	var me = this,
	item = me.focus,
	dataURL = item.canvas.toDataURL();
	item.wrap.removeChild(item.canvas);
	var img = document.createElement('img');
	img.onload = function(){
		var txt = document.createElement('p');
		txt.innerText = 'Conversion to PNG complete. Right Click -> Save Image As';
		item.wrap.appendChild(txt);
		item.wrap.appendChild(img);
		me.resetFocus();
	};
	img.src = dataURL;
};
Glue.prototype.setFocus = function(item){
	this.focus = item;
	item.canvas.style.borderColor = DARKGREY;
}
Glue.prototype.resetFocus = function(){
	this.focus.canvas.style.borderColor = WHITE;
	this.focus = null;
};
Glue.prototype.setToolFocus = function(name, tool){
	this.toolFocus = {
		name: name,
		tool: tool
	};
	tool.el.style.borderColor = DARKGREY;
};
Glue.prototype.checkResetToolFocus = function(){
	if(this.toolFocus !== null){
		this.toolFocus.tool.el.style.borderColor = WHITE;
		//remove all listeners, must come before removing the focus item
		if(typeof this.toolFocus.tool.removeAllListeners === 'function'){
			this.toolFocus.tool.removeAllListeners();
		}
		//remove the focus item
		this.toolFocus.tool.focusItem = null;
		this.toolFocus = null;
	}
};
//Tab functions
function setActiveTabColor(el){
	el.style.borderBottomColor = OFFWHITE;
	el.style.backgroundColor = OFFWHITE;
	
}
function setInactiveTabColor(el){
	el.style.borderBottomColor = DARKGREY;
	el.style.backgroundColor = LIGHTBLUE;
}

//move this generic into the DM class
function swapDisplayProperty(toBeNone, toBeBlock){
	toBeNone.style.display = 'none';
	toBeBlock.style.display = 'block'; 
}

//Tab els and their corresponding content divs
var EDITORTAB = document.getElementById('editorTab');
var EDITOR = document.getElementById('editorContent');
var COMBINATORTAB = document.getElementById('combinatorTab');
var COMBINATOR = document.getElementById('combinatorContent');

//on click, swap the editor and combinator
//TODO: later this can be done with an array of tabs and array.map
EDITORTAB.addEventListener('click', function(e){
	setInactiveTabColor(COMBINATORTAB);
	setActiveTabColor(EDITORTAB);
	swapDisplayProperty(COMBINATOR, EDITOR);
}, false);
COMBINATORTAB.addEventListener('click', function(e){
	setInactiveTabColor(EDITORTAB);
	setActiveTabColor(COMBINATORTAB);
	swapDisplayProperty(EDITOR, COMBINATOR);
}, false);

//set the default tab to editor
setActiveTabColor(EDITORTAB);
swapDisplayProperty(COMBINATOR, EDITOR);


