//Copyright Matthew Parke, Kamaji Apps 2011
var DARKGREY = '#303030';
var WHITE = '#FFFFFF';
var LIGHTBLUE = '#319bd2';
var OFFWHITE = '#f5f5f5';
var WINDOWHEIGHT = window.height;
var WINDOWWIDTH = window.width;

//This inherit function was written incorrectly to simulate multiple inheritance...don't use this.
//Based on douglas crockfords beget object inheritance method:  
//http://javascript.crockford.com/prototypal.html
function inherit(child, parent){
	function F(properties){};
	//this allows for prototype chaining where if we inherit further from the new child, this parent will not acquire the new child's prototype functions
	F.prototype = parent.prototype;
	child.prototype = new F();
};

function isVoid(val){
	return (val === null) || (val === undefined);
};

function createPOSTRequest(url, data, callback){
	var request = new XMLHttpRequest();
	request.open('POST', url, true);
	request.onreadystatechange = function(event){
		console.log('ready state change');
		if(request.readyState === 4){
			//console.log('ready state 4');
			//INVARIANT: the request is finished
			if(request.status === 200){
				//console.log('request state 200');
				//INVARIANT: the request was successful
				callback(request.responseText);
			}
		}
	};
	var stringifiedData = JSON.stringify(data);
	request.send(stringifiedData);
}
//define the class
function Observable(){};
//add prototype methods
Observable.prototype.addEvent = function(eventName){
	//we only want to add if it doesn't already exist, so we don't overwrite
	if(isVoid(this.events[eventName])){
		this.events[eventName] = [];
	}
};
Observable.prototype.addEvents = function(arr){
	var i = 0,
	length = arr.length;
	for(i; i < length; i++){
		this.addEvent(arr[i]);
	}
};
Observable.prototype.addListener = function(eventName, handler, scope){
	if(!isVoid(this.events[eventName])){
		var bound = handler.bind(scope);
		this.events[eventName].push(bound);
	}
};
Observable.prototype.fireEvent = function(eventName, options){
	var i = 0,
	length,
	arr;
	if(!isVoid(this.events[eventName])){
		arr = this.events[eventName];
		length = arr.length;
		for(i; i < length; i++){
			arr[i](options);
		} 
	}
};

//glue is the base class for controlling lists of information
var Glue = function(mainList){
	this.mainList = null;
	this.mainListWrap = null;
	this.items = [];
	this.itemCounter = 0;
	this.focus = null;
	this.toolFocus = null;
	this.events = {}; //important!
};
inherit(Glue, Observable);
//cleanup method
Glue.prototype.varCleanup = function(){
	this.itemCounter = 0;
	this.items.splice(0, this.items.length);
};
Glue.prototype.destroyFocusItem = function(){
	var length = this.items.length,
	item = this.focus;
	item.wrap.removeChild(item.canvas);
	this.mainList.removeChild(item.wrap);
	var i = 0, found = false;
	while(i < length && found === false){
		if(this.items[i].id === item.id){
			this.items.splice(i, 1);
			found = true;
		}
		i++;
	}
	this.resetFocus();
};
//finds the first occurence
//TODO: add options to search from a certain index, or from the back of the array
//returns the index or -1
Glue.prototype.findItem = function(item){
	var i = 0,
	length = this.items.length,
	found = false,
	result = -1;
	while(i < length && !found){
		if(this.items[i].id === item.id){
			result = i;
			found = true;
		}
		i++;
	}
	return result;
};
Glue.prototype.genStructure = function(item){
	//create and append a wrap
	var wrapId = item.id + 'wrap';
	var wrap = DM.genEl('div', null, wrapId, 'wrap'); 
	this.mainList.appendChild(wrap);
	wrap = document.getElementById(wrapId);
	var me = this;
	wrap.addEventListener('click', function(e){
		e.stopPropagation();
		if(me.focus === null){
			me.setFocus(item);
			//in the future we may want additional checks here
	    		//HACK check the id of the focus, before clearing and setting
	 	}else if(me.focus.id !== item.id){
			//INVARIANT: this is not the active focus
			me.resetFocus();
			me.setFocus(item);
			//reset tool focus if canvas focus changes
			me.checkResetToolFocus();
		}
	}, false);
	return wrap;
};
//TODO: subject to change come glue types without canvas elements
Glue.prototype.genCanvas = function(item){
	//create canvas
	var canvasId = item.id + 'canvas';
	var canvas = DM.genEl('canvas', null, canvasId, null);
	canvas.style.borderColor = WHITE;
	item.wrap.appendChild(canvas);
	canvas = document.getElementById(canvasId);
	return canvas;
};
//TODO: subject to change come various glue types?
Glue.prototype.destroyItem = function(){
	var item = this.focus;
	DM.removeChildren(item.wrap);
	this.mainList.removeChild(item.wrap);
	//find the item in the collection and remove it
	var index = this.findItem(item);
	if(index >= 0){
		this.items.splice(index, 1);
	}
};
//TODO: subject to change come glue types without canvas elements
Glue.prototype.convertToPNG = function(){
	var me = this,
	item = me.focus,
	dataURL = item.canvas.toDataURL();
	item.wrap.removeChild(item.canvas);
	var img = document.createElement('img');
	img.onload = function(){
		var txt = document.createElement('p');
		txt.innerText = 'Conversion to PNG complete. Right Click -> Save Image As';
		item.wrap.appendChild(txt);
		item.wrap.appendChild(img);
		me.resetFocus();
	};
	img.src = dataURL;
};
Glue.prototype.setFocus = function(item){
	this.focus = item;
	item.canvas.style.borderColor = DARKGREY;
}
Glue.prototype.resetFocus = function(){
	this.focus.canvas.style.borderColor = WHITE;
	this.focus = null;
};
Glue.prototype.setToolFocus = function(name, tool){
	this.toolFocus = {
		name: name,
		tool: tool
	};
	tool.el.style.borderColor = DARKGREY;
};
Glue.prototype.checkResetToolFocus = function(){
	if(this.toolFocus !== null){
		this.toolFocus.tool.el.style.borderColor = WHITE;
		//remove all listeners, must come before removing the focus item
		if(typeof this.toolFocus.tool.removeAllListeners === 'function'){
			this.toolFocus.tool.removeAllListeners();
		}
		//remove the focus item
		this.toolFocus.tool.focusItem = null;
		this.toolFocus = null;
	}
};
//Tab functions
function setActiveTabColor(el){
	el.style.borderBottomColor = OFFWHITE;
	el.style.backgroundColor = OFFWHITE;
	
}
function setInactiveTabColor(el){
	el.style.borderBottomColor = DARKGREY;
	el.style.backgroundColor = LIGHTBLUE;
}

//move this generic into the DM class
function swapDisplayProperty(toBeNone, toBeBlock){
	toBeNone.style.display = 'none';
	toBeBlock.style.display = 'block'; 
}

//Tab els and their corresponding content divs
var EDITORTAB = document.getElementById('editorTab');
var EDITOR = document.getElementById('editorContent');
var COMBINATORTAB = document.getElementById('combinatorTab');
var COMBINATOR = document.getElementById('combinatorContent');

//on click, swap the editor and combinator
//TODO: later this can be done with an array of tabs and array.map
EDITORTAB.addEventListener('click', function(e){
	setInactiveTabColor(COMBINATORTAB);
	setActiveTabColor(EDITORTAB);
	swapDisplayProperty(COMBINATOR, EDITOR);
}, false);
COMBINATORTAB.addEventListener('click', function(e){
	setInactiveTabColor(EDITORTAB);
	setActiveTabColor(COMBINATORTAB);
	swapDisplayProperty(EDITOR, COMBINATOR);
}, false);

//set the default tab to editor
setActiveTabColor(EDITORTAB);
swapDisplayProperty(COMBINATOR, EDITOR);
//generic dom functions
var DM = {};
//remove all child elements from a list
DM.removeChildren = function(parent){
	while(parent.hasChildNodes()){
        parent.removeChild(parent.firstChild);
    }
};
//typically, first and second are wrap divs
DM.swapChildUp = function(parent, child){
	var previousSibling = child.previousSibling;
	if(previousSibling !== null){
		var tempChild = parent.removeChild(child);
		parent.insertBefore(tempChild, previousSibling);
	}
};
DM.swapChildDown = function(parent, child){
	var nextSibling = child.nextSibling;
	var tempChild = parent.removeChild(child);
	if(nextSibling === null){
		//last in the list, append to parent
		parent.appendChild(tempChild);
	}else{
		parent.insertBefore(tempChild, nextSibling.nextSibling);
	}
};
//create and return a dom element
DM.genEl = function(tag, innerHTML, id, cls){
	var el = document.createElement(tag);
        if(innerHTML != null){
            el.innerHTML = innerHTML;
        }
        if(id != null){
            el.id = id;
        }
        if(cls !=null){
            el.setAttribute('class', cls);
        }
        return el;
};
//creates and returns an anchor element, href required
DM.genAnchor = function(href, innerHTML, id, cls){
        var a = document.createElement('a');
        a.href = href;
        if(innerHTML != null){
            a.innerHTML = innerHTML;
        }
        if(id != null){
            a.id = id;
        }
        if(cls != null){
            a.setAttribute('class', cls);
        }
        return a;
}, 
//creates and returns an image element, src required
DM.genImage = function(src, cls){
	var img = document.createElement('img');
        img.src = src;
        if(cls != null){
            img.setAttribute('class', cls);
        }
        return img;
};
//creates and appends an array of content to a list
DM.genList = function(parent, srcArr, cls){
	var i = 0,
	length = srcArr.length,
	el;
	for(i; i < length; i++){
		el = this.genEl('li', srcArr[i], null, cls);	
		parent.appendChild(el);
	}
};
//creates and appends a number of divs to a parent
DM.genDivs = function(parent, srcArr, cls){
	var i = 0,
	length = srcArr.length,
	el;
	for(i; i < length; i++){
		el = this.genEl('div', srcArr[i], cls);
		parent.appendChild(el);
	}	
};
//creates and returns an array of anchor elements
DM.genAnchors = function(parent, rootId, hrefArr, innerHTMLArr, cls){
	var i = 0,
	length = hrefArr.length,
	results = [],
	id = rootId,
	el;
	for(i; i < length; i++){
		id = id + i;
		el = this.genAnchor(hrefArr[i], innerHTMLArr[i], id, cls);
		parent.appendChild(el);
		el = document.getElementById(rootId);
		results.push(el); 
	}
	return results;
};

DM.getElementPosition = function(el){
	var offsetLeft = 0, offsetTop = 0;
	if (el.offsetParent) {
		do{
			//console.log('setting');
			offsetLeft += el.offsetLeft;
			offsetTop += el.offsetTop;
		}while(el = el.offsetParent);
	}
	return [offsetLeft, offsetTop];
};
//draw Tools
function DrawTool(){
	this.focusItem = null;
};
//clears the canvas
DrawTool.prototype.clearCanvas = function(){
	var item = this.focusItem;
	item.context.clearRect(0, 0, item.canvas.width, item.canvas.height);
};
//reset a item's x and y crop values
DrawTool.prototype.resetXY = function(){
	var item = this.focusItem;
	item.x = 0;
	item.y = 0;
	item.secondX = 0;
	item.secondY = 0;
};
DrawTool.prototype.setHeightAndWidth = function(data){
	var item = this.focusItem;
	item.canvas.height = data.height;
	item.canvas.width = data.width;
};

//a draw actor is a tool which has continous listeners attached to a canvas
function DrawActorTool(){};
inherit(DrawActorTool, DrawTool);
DrawActorTool.prototype.removeAllListeners = function(){
	var canvas = this.focusItem.canvas,
	i = 0,
	length = this.handlers.length,
	handlerObj;
	for(i; i < length; i++){
		handlerObj = this.handlers[i];
		canvas.removeEventListener(handlerObj.name, handlerObj.handler, handlerObj.bubble);
	}
};

function PenTool(){
	this.handlers = [];
};
inherit(PenTool, DrawActorTool);
function SelectionTool(){
	this.handlers = [];
};
inherit(SelectionTool, DrawActorTool);

function CropTool(){};
inherit(CropTool, DrawTool);

//sort tools
function SortTool(){};
//TODO: better index checking
SortTool.prototype.sortArrUp = function(arr, index){
	if(arr[index - 1]){
		var temp = arr[index - 1];
		arr[index - 1] = arr[index];
		arr[index] = temp;
	}
};
//TODO: better index checking
SortTool.prototype.sortArrDown = function(arr, index){
	if(arr[index + 1]){
		var temp = arr[index + 1];
		arr[index + 1] = arr[index];
		arr[index] = temp;
	}
};

PenTool.prototype.turnPenOn = function(){
	var item = this.focusItem;
	var boundMousedownHandler = this.mousedownHandler.bind(this);
	item.canvas.addEventListener('mousedown', boundMousedownHandler, false);
	this.handlers.push({name: 'mousedown', handler: boundMousedownHandler, bubble: false});
	
	var boundMousemoveHandler = this.mousemoveHandler.bind(this);
	item.canvas.addEventListener('mousemove', boundMousemoveHandler, false);
	this.handlers.push({name: 'mousemove', handler: boundMousemoveHandler, bubble: false});
	
	var boundMouseupHandler = this.mouseupHandler.bind(this);
	item.canvas.addEventListener('mouseup', boundMouseupHandler, false);
	this.handlers.push({name: 'mouseup', handler: boundMouseupHandler, bubble: false});
};
PenTool.prototype.mousedownHandler = function(e){
	var item = this.focusItem;
	item.penX.push(e.offsetX);
	item.penY.push(e.offsetY);
	item.drawOn = true;
};
PenTool.prototype.mousemoveHandler = function(e){
	var item = this.focusItem;
	if(item.drawOn){
		item.penX.push(e.offsetX);
		item.penY.push(e.offsetY);
		this.clearCanvas();
		item.context.putImageData(item.dataObjects[item.dataObjects.length - 1], 0, 0);
		item.context.lineJoin = 'round';
		var i = 0, length = item.penX.length;
		for(i; i < length; i++){
				item.context.beginPath();
				item.context.moveTo(item.penX[i - 1], item.penY[i - 1]);
				item.context.lineTo(item.penX[i], item.penY[i]);
				item.context.closePath();
				item.context.stroke();
		}
	}
};
PenTool.prototype.mouseupHandler = function(e){
	var item = this.focusItem;
	var imageData = item.context.getImageData(0, 0, item.canvas.width, item.canvas.height);
	item.dataObjects.push(imageData);
	item.penX.splice(0, item.penX.length);
	item.penY.splice(0, item.penY.length);
	item.drawOn = false;
};


SelectionTool.prototype.turnOnRectangle = function(){
		var item = this.focusItem;
		//set stroke style to refresh ( in case of colored pen )
        item.context.strokeStyle = '#000';
		
		var boundMousedownHandler = this.mousedownHandler.bind(this);
        item.canvas.addEventListener('mousedown', boundMousedownHandler, false);
		this.handlers.push({name: 'mousedown', handler: boundMousedownHandler, bubble: false});
		
		var boundMousemoveHandler = this.mousemoveHandler.bind(this);
        item.canvas.addEventListener('mousemove', boundMousemoveHandler, false);
		this.handlers.push({name: 'mousemove', handler: boundMousemoveHandler, bubble: false});
		
		var boundMouseupHandler = this.mouseupHandler.bind(this);
        item.canvas.addEventListener('mouseup', boundMouseupHandler, false);
		this.handlers.push({name: 'mouseup', handler: boundMouseupHandler, bubble: false});
};
SelectionTool.prototype.mousedownHandler = function(e){
	var item = this.focusItem;
	item.x = e.offsetX;
	item.y = e.offsetY;
	item.drawOn = true;
};
SelectionTool.prototype.mousemoveHandler = function(e){
	var item = this.focusItem;
	if(item.drawOn){
		item.secondX = e.offsetX;
		item.secondY = e.offsetY;
		this.clearCanvas();
		item.context.putImageData(item.dataObjects[item.dataObjects.length - 1], 0, 0);
		item.context.strokeRect(item.x, item.y, item.secondX - item.x, item.secondY - item.y);
	}
};
SelectionTool.prototype.mouseupHandler = function(e){
	var item = this.focusItem;
	item.drawOn = false;
};

CropTool.prototype.doCrop = function(){
	var item = this.focusItem;
	var imageData = item.context.getImageData(item.x, item.y, item.secondX - item.x, item.secondY - item.y); 
	item.dataObjects.push(imageData);
	this.clearCanvas(); 
	this.setHeightAndWidth(imageData);
	item.context.putImageData(imageData, 0, 0);
	item.hasBeenCropped = true;
	this.resetXY();
};
var CG = new Glue();
CG.setup = function(){
	var me = this;
	this.addPublications();
	//this.addSubscriptions();
	
	//setup mainList
	this.mainList = document.getElementById('editorMainList');
	
	//get main list wrap and set focus handler
	this.mainListWrap = document.getElementById('editorMainWrap');
	this.mainListWrap.addEventListener('click', function(e){
		if(me.focus !== null){
			me.resetFocus();
		}
	}, false);
	
	//setup tools
	this.setupSelectionTool();
	this.setupCropTool();
	this.setupPenTool();
	this.setupAddToCombinatorTool();
	this.setupSaveTool();
	this.setupDestroyTool();
	//this.setupUploadTool();		
};
CG.addPublications = function(){
	this.addEvent('addToCombinator');
}
//CG.addSubscriptions = function(){}
CG.setupSelectionTool = function(){
	var selectionEl = document.getElementById('selection');
	var selectionTool = new SelectionTool();
	selectionTool.el = selectionEl;
	var me = this;
	selectionTool.el.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.setToolFocus('selection', selectionTool);
			me.selectionTool.focusItem = me.focus;
			me.selectionTool.turnOnRectangle();
			me.selectionTool.el.style.borderColor = DARKGREY;
			_gaq.push(['_trackEvent', 'selectionTool', 'clicked']);
		}
	}, false);
	me.selectionTool = selectionTool;
};
CG.setupCropTool = function(){
	//get tools and set focus handler
	var cropEl = document.getElementById('crop');
	var cropTool = new CropTool();
	cropTool.el = cropEl;
	var me = this;
	cropTool.el.addEventListener('click', function(){
		//we can check toolFocus because undefined returns falsy
		if(!isVoid(me.toolFocus) && me.toolFocus.name === 'selection'){
			if(!isVoid(me.focus)){
				me.checkResetToolFocus();
				//because the crop tool doesn't have any event handlers, we never set toolFocus on him
				me.cropTool.focusItem = me.focus;
				me.cropTool.doCrop();
				me.focus.wrap.style.width = me.focus.canvas.style.width;
				me.resetFocus();
				_gaq.push(['_trackEvent', 'cropTool', 'clicked']);
			}
		}
	}, false);
	me.cropTool = cropTool;
};
CG.setupPenTool = function(){
	var me = this;
	var penEl = document.getElementById('pen');
	var penTool = new PenTool();
	penTool.el = penEl;
	penTool.el.addEventListener('click', function(e){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.setToolFocus('pen', me.penTool);
			me.penTool.focusItem = me.focus;
			me.penTool.turnPenOn();
			_gaq.push(['_trackEvent', 'penTool', 'clicked']);
		}
	}, false);
	this.penTool = penTool;
};
CG.setupAddToCombinatorTool = function(){
	var me = this;
	var addToCombinatorEl = document.getElementById('addToCombinator');
	addToCombinatorEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var item = me.focus;
			var height = item.canvas.height;
			var width = item.canvas.width;
			//get the current state of the image
			var imageData = item.context.getImageData(0, 0, width, height);
			me.fireEvent('addToCombinator', {imageData: imageData, height: height, width: width});
			me.resetFocus();
			_gaq.push(['_trackEvent', 'addToCombinatorTool', 'clicked']);
		}
	}, false);
	this.addToCombinatorEl = addToCombinatorEl;
};
//TODO save
CG.setupSaveTool = function(){
	var me = this;
	var saveEl = document.getElementById('save');
	saveEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.convertToPNG();
			_gaq.push(['_trackEvent', 'saveTool', 'clicked']);
		}
	}, false);
	this.saveEl = saveEl;
};
CG.setupDestroyTool = function(){
	var me = this;
	var destroyEl = document.getElementById('destroy');
	destroyEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.destroyFocusItem();
			_gaq.push(['_trackEvent', 'destroyTool', 'clicked']);
		}
	}, false);
	this.destroyEl = destroyEl;
};
CG.setupUploadTool = function(){
	var me = this;
	var upload = document.getElementById('getURL');
	upload.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			var item = me.focus;
            var dataURL = item.canvas.toDataURL();
			//outgoing data
			var dataChunk = {
				dataURL: dataURL,
				sourceURL: item.sourceURL,
				pageTitle: item.pageTitle
			};
			//form an http request to the server.
			var url = 'http://canvasglue.com/upload';
			createPOSTRequest(url, dataChunk, function(responseText){
				var linkWrapId = item.id + 'imageLinkWrap';
				var div = DM.genEl('div', null, linkWrapId, null);
				item.wrap.insertBefore(div, item.canvas);
				div = document.getElementById(linkWrapId);
				
				var anchor = DM.genAnchor(responseText, responseText, null, null);
				div.appendChild(anchor);
			});
			_gaq.push(['_trackEvent', 'uploadTool', 'clicked']);
		}
	}, false);
};
//returns a blank item object
CG.genItem = function(){
	var item = {
		id: this.itemCounter,
		dataURL: null,
		x: 0,
		y: 0,
		secondX: 0,
		secondY: 0,
		penX: [],
		penY: [],
		varsSet: false,
		penVarsSet: false,
		drawOn: false,
		cropOn: false,
		combinatorNum:0,
		dataObjects: [],
		wrap: null,
		canvas: null,
		context: null
				
	};
	++this.itemCounter;
	return item;
};
CG.genBlank = function(){
	var item = this.genItem();
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawBlankImage(item);
};
CG.genWholePage = function(dataURLArr, sourceURL, pageTitle){
	var i = 0, length = dataURLArr.length;
	for(i; i < length; i++){
		this.genScreenshot(dataURLArr[i], sourceURL, pageTitle);
	}
};
CG.genScreenshot = function(dataURL, sourceURL, pageTitle){
	var item = this.genItem();
	item.dataURL = dataURL;
	item.sourceURL = sourceURL;
	item.pageTitle = pageTitle;
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawImage(item);
	_gaq.push(['_trackEvent', 'genScreenshot', 'screenshotGenerated']);
};
CG.drawImage = function(item){
	var me = this;
	
	//get the canvas context
	var context = item.canvas.getContext('2d');
	item.context = context;
	
	var image = new Image();
	image.onload = function(){
		var width = image.width;
		var height = image.height;
		item.wrap.style.height = height;
		item.wrap.style.width = width;
		item.canvas.height = height;
		item.canvas.width = width;
		context.drawImage(image, 0, 0);
		//save image state and item
		me.saveItem(item);
		//clear the dataUrl
		delete me.dataUrl;
	};
	image.src = item.dataURL;
};
CG.drawBlankImage = function(item){
	var width = 900, 
	height = 900,
	context = item.canvas.getContext('2d');
	item.context = context;
	
	item.wrap.style.height = height;
	item.wrap.style.width = width; 
	item.canvas.height = height;
	item.canvas.width = width;
	
	context.fillStyle = WHITE;
	//top x left, top y left, width, height
	context.fillRect(0, 0, width, height);
	
	//save image state and item
	this.saveItem(item);
};
CG.saveItem = function(item){
	//save image state
	var imageData = item.context.getImageData(0, 0, item.canvas.width, item.canvas.height);
	item.dataObjects.push(imageData);
	
	//save item
	this.items.push(item);
};
CG.setup();
//combinator handles everything in imageData
var CMB = new Glue();
CMB.setup = function(){
	var me = this;
	me.itemCounter = 100;
	
	//TODO: setup notifcation
	//me.notify = document.getElementById('combinatorNotify');
	//add pub sub
	//this.addPublications();
	this.addSubscriptions();
	
	this.mainList = document.getElementById('combinatorMainList');
	this.mainListWrap = document.getElementById('combinatorMainWrap');
	this.mainListWrap.addEventListener('click', function(e){
		if(!isVoid(me.Focus) && !isVoid(me.toolFocus)){
			me.resetFocus();
		}
	}, false);
	
	//setup tools
	this.setupSortTool();
	this.setupMergeTool();
	this.setupSaveTool();
	this.setupDestroyTool();
};
//CMB.addPublications = function(){}
CMB.addSubscriptions = function(){
	CG.addListener('addToCombinator', this.addImage, this);
};
CMB.setupSortTool = function(){
	var me = this;
	var sortUpEl = document.getElementById('sortUp');
	var sortDownEl = document.getElementById('sortDown');
	var sortTool = new SortTool();
	sortTool.upEl = sortUpEl;
	sortTool.downEl = sortDownEl;
	sortTool.upEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var index = me.findItem(me.focus);
			if(index !== -1){
				DM.swapChildUp(me.mainList, me.focus.wrap);
				sortTool.sortArrUp(me.items, index);
			}
			_gaq.push(['_trackEvent', 'CMBSortUpTool', 'clicked']);
		}
	}, false);
	sortTool.downEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			var index = me.findItem(me.focus);
			if(index !== -1){
				DM.swapChildDown(me.mainList, me.focus.wrap);
				sortTool.sortArrDown(me.items, index);	
			}
			_gaq.push(['_trackEvent', 'CMBSortDownTool', 'clicked']);
		}
	}, false);
};
CMB.setupMergeTool = function(){
	var me = this;
	var mergeEl = document.getElementById('merge');
	mergeEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.mergeItems();
			me.resetFocus();
			_gaq.push(['_trackEvent', 'CMBMergeTool', 'clicked']);
		}
	}, false);
};
CMB.setupSaveTool = function(){
	var me = this;
	var saveEl = document.getElementById('combinatorSave');
	saveEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.convertToPNG();
			me.resetFocus();
			_gaq.push(['_trackEvent', 'CMBSaveTool', 'clicked']);
		}
	}, false);
	this.saveEl = saveEl;
};
CMB.setupDestroyTool = function(){
	var me = this;
	var destroyEl = document.getElementById('combinatorDestroy');
	//me.addEvent('destroy');
	destroyEl.addEventListener('click', function(){
		if(!isVoid(me.focus)){
			me.checkResetToolFocus();
			me.destroyFocusItem();
			_gaq.push(['_trackEvent', 'CMBDestroyTool', 'clicked']);
		}
	}, false);
	this.destroyEl = destroyEl;
};
CMB.addImage = function(data){
	var item = {
		id: this.itemCounter,
		imageData: data.imageData,
		height: data.height,
		width: data.width,
		wrap: null,
		canvas: null,
		context: null
	};
	++this.itemCounter;
	var wrap = this.genStructure(item);
	item.wrap = wrap;
	var canvas = this.genCanvas(item);
	item.canvas = canvas;
	this.drawImage(item);
	this.items.push(item);
};
CMB.drawImage = function(item){
	item.canvas.height = item.height;
	item.canvas.width = item.width;
	var context = item.canvas.getContext('2d');
	item.context = context;
	
	context.putImageData(item.imageData, 0, 0);
};
CMB.mergeItems = function(){
	var i = 0,
	length = this.items.length,
	maxWidth = 0,
	totalHeight = 0,
	incrementalHeight = 0,
	item,
	dataObject;
	//TODO: check if we should remove canvases first, before removing the wrapping divs
	DM.removeChildren(this.mainList);
	
	//find max width and height
	//TODO: use array.reduce
	for(i; i < length; i++){
		item = this.items[i];
		if(item.width > maxWidth){
			maxWidth = item.width;
		}
		totalHeight = totalHeight + item.height;
	}
	
	//create a special wrap and canvas for the merge
	var wrap = DM.genEl('div', null, 'mergeWrap', 'wrap');
	wrap.style.height = totalHeight;
	wrap.style.width = maxWidth;
	this.mainList.appendChild(wrap);
	wrap = document.getElementById('mergeWrap');
	//create message
	var msg = document.createElement('p');
	msg.innerText = 'Combination status: success.';
	wrap.appendChild(msg);
	//create canvas
	var canvas = DM.genEl('canvas', null, 'mergeCanvas', null);
	canvas.height = totalHeight;
	canvas.width = maxWidth;
	canvas.style.borderColor = WHITE;
	wrap.appendChild(canvas);
	canvas = document.getElementById('mergeCanvas');
	
	var context = canvas.getContext('2d');
	i = 0;
	//TODO: extra array indices checks?
	for(i; i < length; i++){
		item = this.items[i];
		if(i === 0){
			context.putImageData(item.imageData, 0, 0);
			incrementalHeight = item.height;
		}else{
			context.putImageData(item.imageData, 0, incrementalHeight);
			incrementalHeight = incrementalHeight + item.height;
		}
	}
	
	//cleanup
	this.varCleanup();
	
	//push new item so that we can save it
	var item = {
		id: 'merged',
		imageData: context.getImageData(0, 0, canvas.width, canvas.height),
		//TODO: check if height and width are used
		height: canvas.height,
		width: canvas.width,
		wrap: wrap,
		canvas: canvas,
		context: context
	};
	this.items.push(item);
	this.checkResetToolFocus();
	this.focus = item;
	this.convertToPNG();
};
CMB.setup();