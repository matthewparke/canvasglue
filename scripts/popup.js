//analytics
var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-22460819-2']);
 _gaq.push(['_trackPageview']);
 
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

var capture = document.getElementById("capture");
var capVis = function(){ 
	chrome.extension.sendRequest({type:"capture"}, function(response) {});
	_gaq.push(['_trackEvent', 'captureScreenshot', 'clicked']);
};
capture.addEventListener('mousedown', capVis, false);

var captureWhole = document.getElementById("captureWhole");
var wholePageTraversal = function(){
	chrome.extension.sendRequest({type:"captureWhole"}, function(response) {});
	_gaq.push(['_trackEvent', 'captureWholePage', 'clicked']);
};
captureWhole.addEventListener('mousedown', wholePageTraversal, false);

var crawl = document.getElementById("crawl");
var crawlAnchors = function(){
	chrome.extension.sendRequest({type:"crawl"}, function(response) {});
	_gaq.push(['_trackEvent', 'crawlLinkedImages', 'clicked']);
};
crawl.addEventListener('mousedown', crawlAnchors, false);

var blank = document.getElementById("blank");
var genBlank = function(){
	chrome.extension.sendRequest({type:"genBlank"}, function(response) {});
	_gaq.push(['_trackEvent', 'blankCanvas', 'clicked']);
};
blank.addEventListener('mousedown', genBlank, false);